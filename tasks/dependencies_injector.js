/*
 * grunt-dependencies-injector
 * 
 *
 * Copyright (c) 2015 Lorenzo Savini
 * Licensed under the Apache-2.0 license.
 */

'use strict';

module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-ng-annotate');

    /**
     * A utility function to get all app JavaScript sources.
     */
    function filterForJS (files) {
        return files.filter( function ( file ) {
            return file.match( /\.js$/ );
        });
    }

    /**
     * A utility function to get all app CSS sources.
     */
    function filterForCSS (files) {
        return files.filter( function ( file ) {
            return file.match( /\.css$/ );
        });
    }

    function filterForVendor (file) {
        return file.match(/bower_components/);
    }

    function filterForApp (file) {
        return !file.match(/bower_components/);
    }

    grunt.registerMultiTask('index_compiler', '', function() {
        /**
         * The index.html template includes the stylesheet and javascript sources
         * based on dynamic names calculated in this Gruntfile. This task assembles
         * the list into variables for the template to use and then runs the
         * compilation.
         */
        var vendorCssFiles  = filterForCSS(this.filesSrc).filter(filterForVendor).reverse();
        var cssFiles        = filterForCSS(this.filesSrc).filter(filterForApp).reverse();
        var vendorJsFiles   = filterForJS(this.filesSrc).filter(filterForVendor).reverse();
        var jsFiles         = filterForJS(this.filesSrc).filter(filterForApp).reverse();

        var pkgVersion      = this.data.pkg_version;

        grunt.file.copy('src/index.html', this.data.dir + '/index.html', {
            process: function(contents, path) {
                return grunt.template.process(contents, {
                    data: {
                        version: pkgVersion,
                        vendor_styles: vendorCssFiles,
                        styles: cssFiles,
                        vendor_scripts: vendorJsFiles,
                        scripts: jsFiles
                    }
                });
            }
        });
        grunt.log.oklns(this.data.dir+"/index.html compiled with the package version " +pkgVersion+ ": ");
        grunt.log.writeln((vendorCssFiles.length+cssFiles.length) + " CSS files included");
        grunt.log.writeln((vendorJsFiles.length+jsFiles.length) + " JS files included");
    });

    grunt.registerMultiTask('dependencies_injector', 'A plugin that automate assets inclusion into html files', function() {

        (this.target=="build")
            ? grunt.task.run(['dependencies_build', 'index_compiler:build'])
            : grunt.task.run(['dependencies_compile', 'index_compiler:compile', 'uglify']);

    });

    /**
     * 'dependencies_build' task
     */
    grunt.registerTask('dependencies_build', [
        'clean:build',
        'concat:build_css',
        'copy:build_app_assets',
        'copy:build_vendor_assets',
        'copy:build_appjs',
        'copy:build_vendorjs'
    ]);

    /**
     * 'dependencies_compile' task
     */
    grunt.registerTask( 'dependencies_compile', [
        'clean:bin',
        'ngAnnotate',
        'concat:compile_js'
    ]);


};
