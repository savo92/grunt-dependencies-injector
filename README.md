# grunt-dependencies-injector

> A plugin that automate assets inclusion into html files

Normally, this plugin is used with DocLine but you can use this plugin in any other project.

**IMPORTANT** The develpment of this plugin was started inside DocLine but it was refactored  
to be standalone from DocLine. Until the end of the DocLine's development  
this plugin will be updated to work with DocLine. After the end of the development,  
we will work more on this

## Getting Started
This plugin requires Grunt.

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```
npm install grunt-dependencies-injector --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```
grunt.loadNpmTasks('grunt-dependencies-injector');
```

## The "dependencies_injector" task

### Overview
In your project's Gruntfile, add a section named `dependencies_injector` to the data object passed into `grunt.initConfig()`.  
__Look the configuration is for the index\_compiler task that is one of the task run by dependencies\_injector__ 

```
grunt.initConfig({
    index_compiler: {
        build: {
            dir: '',
            pkg_version: '',
            src: [
            ]
        },
        compile: {
            dir: '',
            pkg_version: '',
            src: [
            ]
        }
    },
    dependencies_injector: {
        build: {},
        compile: {}
    }
})
```

### Options

You can customize the settings for the selected target 'build' or 'compile'

#### build.dir
Type: `String`
Where locate the processed index.html 

#### build.pkg_version
Type: `String`
The version to use for building files

#### build.src
Type `Array`
A list of paths where find the assets to include in the processed index.html file

#### compile.dir
Type: `String`
Where locate the processed index.html 

#### compile.pkg_version
Type: `String`
The version to use for building files

#### compile.src
Type `Array`
A list of paths where find the assets to include in the processed index.html file


### Usage Examples

#### Default Options
These are the plugin's main configurations but there are many others sub-configurations for  
the 'clean', 'copy', 'concat', 'ngAnnotate', 'uglify' tasks. These tasks use some configurations to find the  
resources. Please check the Wiki of this repository.

```
grunt.initConfig({
    index_compiler: {
        /**
         * During development, we don't want to have wait for compilation,
         * concatenation, minification, etc. So to avoid these steps, we simply
         * add all script files directly to the '<head>' of 'index.html'. The
         * 'src' property contains the list of included files.
         */
        build: {
            dir: '.',
            pkg_version: '<%= pkg.version %>',
            src: [
                '<%= build_dir %>/**/*.css',
                '<%= build_dir %>/**/*.js'
            ]
        },
        /**
         * When it is time to have a completely compiled application, we can
         * alter the above to include only a single JavaScript and a single CSS
         * file. Now we're back!
         */
        compile: {
            dir: '.',
            pkg_version: '<%= pkg.version %>',
            src: [
                '<%= vendor_files.css %>',
                '<%= concat.compile_js.dest %>'
            ]
        }
    },
    dependencies_injector: {
        build: {},
        compile: {}
    }
})
```

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## License
Copyright (c) 2015 Lorenzo Savini. Licensed under the Apache-2.0 license.